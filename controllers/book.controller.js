const db = require("../models");
const Book = db.book;

exports.all = (req, res) => {

    let { page = 1, limit = 5} = req.query;

    Book.findAndCountAll({
        limit: parseInt(limit),
        offset: parseInt((page - 1) * limit),
    })
    .then(data => {
        res.send(data);
    })
    .catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });  
};

exports.create = (req, res) => {
    Book.create({
        name: req.body.name,
        pages: req.body.pages,
    })
    .then(() => {
        res.send({ message: "Book has beed successfully saved!" });
    }).catch((err)=>{
        res.status(500).send({ message: err.message });
    });
};

exports.read = (req, res) => {

    Book.findOne({
        where: {
          id: req.params.id
        }
    })
    .then((data) => {
        res.send(data);
    })
    .catch((err)=>{
        res.status(500).send({ message: err.message });
    });

};

exports.update = (req, res) => {
    Book.update({
        name: req.body.name,
        pages: req.body.pages
    }, {
        where: {
          id : req.params.id
        }
    })
    .then(() => {
        res.send({ message: "Book has beed successfully updated!" });
    })
    .catch((err)=>{
        res.status(500).send({ message: err.message });
    });
};

exports.destroy = (req, res) => {
    Book.destroy({
        where: {
            id: req.params.id
        }
    })
    .then(() => {
        res.send({ message: "Book has beed successfully deleted!" });
    })
    .catch((err)=>{
        res.status(500).send({ message: err.message });
    });
};