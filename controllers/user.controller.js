const db = require("../models");
const User = db.user;


exports.allAccess = (req, res) => {
  res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {

  User.findAndCountAll({
    limit: 5,
    offset: 0,
  })
  .then(data => {
    console.log(data);
    res.send(data);
  })
  .catch(err => {
    res.status(500).send({ message: err.message });
  });  
};

exports.adminBoard = (req, res) => {
  res.status(200).send("Admin Content.");
};
