const { authJwt } = require("../middleware");
const controller = require("../controllers/book.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/book/all", controller.all);

  app.post(
    "/api/book/create",
    [authJwt.verifyToken],
    controller.create
  );

  app.get(
    "/api/book/read/:id",
    [authJwt.verifyToken],
    controller.read
  );

  app.patch(
    "/api/book/update/:id",
    [authJwt.verifyToken],
    controller.update
  );

  app.delete(
    "/api/book/delete/:id",
    [authJwt.verifyToken],
    controller.destroy
  );
};
