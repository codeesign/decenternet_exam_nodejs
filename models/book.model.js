module.exports = (sequelize, Sequelize) => {
    const Book = sequelize.define("books", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING
      },
      pages: {
        type: Sequelize.INTEGER
      }
    });
  
    return Book;
  };
  